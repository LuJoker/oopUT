#include <iostream>
#include <vector>
#include<cppunit/TestCase.h>
#include<cppunit/TestFixture.h>
#include<cppunit/TestCaller.h>
#include<cppunit/TestResult.h>
#include<cppunit/TestResultCollector.h>
#include<cppunit/XmlOutputter.h>
#include<cppunit/ui/text/TestRunner.h>

#include "ToDoList.h"

using namespace std;

//unit test for to do list
class ToDoListTest : public CppUnit::TestFixture
{
    public:
    ToDoList *my_tasks;
    void setUp()
    {
        my_tasks=new ToDoList();
    }
    void tearDown()
    {
        delete my_tasks;
    }
    void test_add_normal_task()
    {
        bool result = my_tasks->add_new_task("Write 2130");
        CPPUNIT_ASSERT(result=true);

    }
    void test_add_empty_task()
    {
        bool result = my_tasks->add_new_task("");
        CPPUNIT_ASSERT(result=false);

    }
    static CppUnit::Test* suite()
    {
        CppUnit::TestSuite *suite_of_tests = new CppUnit::TestSuite("ToDoList Test");
        suite_of_tests->addTest(new CppUnit::TestCaller<ToDoListTest>("test Add normal task",&ToDoListTest::test_add_normal_task));
        suite_of_tests->addTest(new CppUnit::TestCaller<ToDoListTest>("test Add empty task",&ToDoListTest::test_add_normal_task));
        return suite_of_tests;
    }


};

int main()
{
    CppUnit::TestResult testresult;
    CppUnit::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    CppUnit::TextUi::TestRunner runner;
    runner.addTest(ToDoListTest::suite());
    runner.run(testresult);

    std::ofstream xmlFileOut("CppUnitTestDmServerResults.xml");
    CppUnit::XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    return 0;
}
